// Módulos js: https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Statements/import
//             https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Modules#aplicar_el_m%C3%B3dulo_a_tu_html
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";

const firebaseConfig = {
    apiKey: "AIzaSyB4IA8OWMQWagZN4BrVaTdXoZYOJOAlF_I",
    authDomain: "sandra-e926f.firebaseapp.com",
    databaseURL: "https://sandra-e926f-default-rtdb.firebaseio.com",
    projectId: "sandra-e926f",
    storageBucket: "sandra-e926f.appspot.com",
    messagingSenderId: "307134148110",
    appId: "1:307134148110:web:1f878635a55daa3c00565f"
  };

const app = initializeApp(firebaseConfig);

console.log("Consola de pruebas...");

// Referencias al HTML
let correoRef = document.getElementById("direccionCorreoId");
let passRef = document.getElementById("passwordId");
let buttonRef = document.getElementById("altaButtonId");
let ingresarRef = document.getElementById("ingresarButtonId");

// Event Listeners
buttonRef.addEventListener("click", altaUsuario);
ingresarRef.addEventListener("click", logIn);

const auth = getAuth();

// Promesas: https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Using_promises
//           https://developer.mozilla.org/es/docs/Glossary/Callback_function
//           https://www.youtube.com/watch?v=slIJj-zbs_M

function altaUsuario(){

    console.log("Ingreso a la función altaUsuario().");

    if((correoRef.value != '') && (passRef.value != '')){

        // API Docs. https://firebase.google.com/docs/reference/js/auth.md?authuser=0&hl=en#createuserwithemailandpassword
        // let variable = createUserWithEmailAndPassword(auth, correoRef.value, passRef.value);
        // console.log(variable);

        // https://www.w3schools.com/js/js_arrow_function.asp
        createUserWithEmailAndPassword(auth, correoRef.value, passRef.value)
        .then((userCredential) => {
            // Signed in
            const user = userCredential.user;
            console.log("Usuario: " + user + " ID: " + user.uid);
            console.log("Creación de usuario.");

            correoRef.value = '';
            passRef.value = '';
            window.location.href = "./informacion.html";

        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);

            // if(errorCode == 'auth/email-already-in-use'){
            //     alert("Mail ya empleado por otro usuario.");
            // }
        });
    }
    else{
        alert("Revisar que los campos de usuario y contraseña esten completos.");
    }    

}

function logIn (){

    if((correoRef.value != '') && (passRef.value != '')){

        signInWithEmailAndPassword(auth, correoRef.value, passRef.value)
        .then((userCredential) => {
            // Signed in
            const user = userCredential.user;
            window.location.href = "./informacion.html";
            // ...
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
        });
    }
    else{
        alert("Revisar que los campos de usuario y contraseña esten completos.");
    }    

}






