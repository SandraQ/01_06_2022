console.log("Consola de pruebas del chat");

import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getDatabase, ref, set, push, onValue } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";

const firebaseConfig = {
    apiKey: "AIzaSyB4IA8OWMQWagZN4BrVaTdXoZYOJOAlF_I",
    authDomain: "sandra-e926f.firebaseapp.com",
    databaseURL: "https://sandra-e926f-default-rtdb.firebaseio.com",
    projectId: "sandra-e926f",
    storageBucket: "sandra-e926f.appspot.com",
    messagingSenderId: "307134148110",
    appId: "1:307134148110:web:1f878635a55daa3c00565f"
};

const app = initializeApp(firebaseConfig);

// Get a reference to the database service
const database = getDatabase(app);

//referencias al DOM

let mensajeRef = document.getElementById("textoChatId");
let botonRef = document.getElementById("buttonnChatId");
let chatRef = document.getElementById("mensajeChatId");

botonRef.addEventListener("click", cargarMensaje);

function cargarMensaje(

    let mensaje = mensajeRef.ref
    set(ref(database, "mensaje/"), 
    {
    msg: mensaje
    })
)

onValue(ref(database, 'mensaje/'), (snapshot) => {
    const lectura = snapshot.val();

    console.log(lectura.data);

    //    chatRef.innerHTML = chatRef.innerHTML + lectura.msg;
    //Para insertar una funcion de HTML a JS, uso las ``, y entre las mismas, lo de ${}
    chatRef.innerHTML += `
        <p>${lectura.msg}/p>
    `;
});










