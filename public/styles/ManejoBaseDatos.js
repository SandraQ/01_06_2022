console.log("Consola de pruebas...");

import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getDatabase, ref, set, push, onValue } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";

const firebaseConfig = {
    apiKey: "AIzaSyB4IA8OWMQWagZN4BrVaTdXoZYOJOAlF_I",
    authDomain: "sandra-e926f.firebaseapp.com",
    databaseURL: "https://sandra-e926f-default-rtdb.firebaseio.com",
    projectId: "sandra-e926f",
    storageBucket: "sandra-e926f.appspot.com",
    messagingSenderId: "307134148110",
    appId: "1:307134148110:web:1f878635a55daa3c00565f"
  };

const app = initializeApp(firebaseConfig);

// Get a reference to the database service
const database = getDatabase(app);

// Referencias al HTML
let textoRef = document.getElementById("textoBaseDatosId");
let botonRef = document.getElementById("buttonBaseDatosId");
let valorDataBaseRef = document.getElementById("valorDataBaseId");

// Event Listeners
botonRef.addEventListener("click", cargarInformacion);

// Función de carga de información empleando el método set de la API de Firebase Realtime Database
function cargarInformacion(){
    let informacion = textoRef.value;

    // API Docs.: https://firebase.google.com/docs/reference/js/database.md?hl=en#set
    set(ref(database, 'datos/'), {
        data: informacion,
        nombre: "Sandra",
        apellido: "Quispe",
        domicilio: "Calle divertida 123"
      });

    console.log("Carga de información correcta.");
}

// API Docs.: https://firebase.google.com/docs/reference/js/database.md?hl=en#onvalue
onValue(ref(database, 'datos/'), (snapshot) => {
    const lectura = snapshot.val();

    console.log(lectura.data);

    valorDataBaseRef.innerHTML = lectura.data;
});
